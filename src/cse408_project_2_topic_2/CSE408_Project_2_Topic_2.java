/*
 * CSE 408
 * Project 2
 * Topic 2
 *
 * Xiangzhi Meng
 * Kang Piao
 * Fumi Honda
 *
 * 4/21/2015
 */
package cse408_project_2_topic_2;

import com.jamesmurty.utils.XMLBuilder;
import java.awt.GridLayout;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import static java.lang.System.out;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.xml.parsers.FactoryConfigurationError;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * @author xiangzhimeng, Kang Piao, Fumi Honda
 */
public class CSE408_Project_2_Topic_2 {

    private static JPanel panel;

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        // Property for the output XML data collction file.
        Properties outputProperties = new Properties();
        // Explicitly identify the output as an XML document
        outputProperties.put(javax.xml.transform.OutputKeys.METHOD, "xml");
        // Pretty-print the XML output (doesn't work in all cases)
        outputProperties.put(javax.xml.transform.OutputKeys.INDENT, "yes");

        BufferedReader br = null;
        BufferedReader regexBr = null;
        PrintWriter XMLWriter = null;
        PrintWriter domTreeDumpWriter = null;
        try {
            File file = new File("input.txt");
            File regexFile = new File("regexInput.txt");

            br = new BufferedReader(new FileReader(file));
            regexBr = new BufferedReader(new FileReader(regexFile));
            XMLWriter = new PrintWriter(new FileOutputStream("data_collections//dataCollection.xml"));

            String urlToVisit = br.readLine();

            XMLBuilder builder = XMLBuilder.create("Data_Collection");

            int webPageCounter = 1;
            while (urlToVisit != null) { // For each URL in the input file

                // Launch the Chrome in the mobile emulation mode
                Map<String, String> mobileEmulation = new HashMap<>();
                mobileEmulation.put("deviceName", "Google Nexus 5");

                Map<String, Object> chromeOptions = new HashMap<>();
                chromeOptions.put("mobileEmulation", mobileEmulation);
                DesiredCapabilities capabilities = DesiredCapabilities.android();
                capabilities.setCapability(ChromeOptions.CAPABILITY, chromeOptions);
                WebDriver driver = new ChromeDriver(capabilities);

                // Visit the web site
                driver.get(urlToVisit);

                // Each dom tree dump writer will dump the page source to a html file
                domTreeDumpWriter = new PrintWriter(new FileOutputStream("data_collections//webPage" + webPageCounter + ".html"));

                // Record in the xml each website visited
                builder.e("URL").t(driver.getCurrentUrl());

                // Record the popup box text if there is any
                builder.e("alert_text").t(getAlertText(driver)).up();

                // Dump the dom tree to a html file
                domTreeDumpWriter.print(driver.getPageSource());

                // Take the screenshot of the web page
                File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
                FileUtils.copyFile(scrFile, new File("data_collections//webPageScreenshot" + webPageCounter + ".png"));

                if (domTreeDumpWriter != null) {
                    domTreeDumpWriter.close();
                }

                for (int pos = 0; pos < getNumberOfElementsFound(By.tagName("iframe"), driver); pos++) {

                    // Find the element by the index
                    WebElement e = getElementWithIndex(By.tagName("iframe"), pos, driver);

                    if (e.isDisplayed()) {
                        try {
                            // Record each of the web element
                            builder.e("iframe").t(e.getAttribute("src")).up();

                            //FH{
                            //Before clickin on iframe's src check if the url matches any in blacklist
                            String srcString = e.getAttribute("src");
                            if (isMaliciousAd(srcString)) {
                                //call warning message box
                                panel = new JPanel(new GridLayout(2, 2));
                                int option = JOptionPane.showConfirmDialog(null, panel, "Malicious Ad detected! Proceed at your own risk?", JOptionPane.DEFAULT_OPTION);

                                if (option == JOptionPane.OK_OPTION) {
                                    e.click();
                                    System.out.println("User clicked OK!");
                                }
                            } else {  //if not malicious Ad
                                // Click on the iframe to open the ads
                                e.click();
                            }
                            //FH}
                        } catch (Exception ex) {
                            // Record each popup box text if there is any
                            builder.e("iframeNotClickable").t(ex.getMessage()).up();
                        }
                    }
                }

                ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
                int iframeCounter = 1;

                // Record the DOM tree dump for each ad opened
                for (String tab : tabs) {
                    driver.switchTo().window(tab);
                    // Each dom tree dump writer will dump the page source to a html file
                    domTreeDumpWriter = new PrintWriter(new FileOutputStream("data_collections//iframeWebPage" + webPageCounter
                            + "_" + iframeCounter + ".html"));
                    // Dump the dom tree to a html file
                    domTreeDumpWriter.print(driver.getPageSource());

                    // Record each redirection
                    builder.e("Redirection").t(driver.getCurrentUrl()).up();

                    // Record each popup box text if there is any
                    builder.e("alert_text").t(getAlertText(driver)).up();

                    if (domTreeDumpWriter != null) {
                        domTreeDumpWriter.close();
                    }
                    iframeCounter++;
                }

                iframeCounter = 1;

                // Take the screenshot for each ads opened
                for (String tab : tabs) {
                    driver.switchTo().window(tab);

                    // Take the screenshot of the ads
                    scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
                    FileUtils.copyFile(scrFile, new File("data_collections//iframeScreenshot" + webPageCounter
                            + "_" + iframeCounter + ".png"));
                    driver.close();
                    iframeCounter++;
                }
                driver.quit();
                builder.up();
                // Read the next URL
                urlToVisit = br.readLine();

                webPageCounter++;
            }
            builder.up();
            builder.toWriter(XMLWriter, outputProperties);
            if (XMLWriter != null) {
                XMLWriter.close();
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(CSE408_Project_2_Topic_2.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(CSE408_Project_2_Topic_2.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ParserConfigurationException | FactoryConfigurationError | TransformerException ex) {
            Logger.getLogger(CSE408_Project_2_Topic_2.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                if (br != null) {
                    br.close();
                }
                if (regexBr != null) {
                    regexBr.close();
                }
                if (XMLWriter != null) {
                    XMLWriter.close();
                }
                if (domTreeDumpWriter != null) {
                    domTreeDumpWriter.close();
                }
            } catch (IOException ex) {
                Logger.getLogger(CSE408_Project_2_Topic_2.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    /**
     * This function get the text from the alert box.
     *
     * @param driver
     * @return
     */
    public static String getAlertText(WebDriver driver) {
        String text = "";
        try {
            WebDriverWait wait = new WebDriverWait(driver, 1);
            wait.until(ExpectedConditions.alertIsPresent());
            Alert alert = driver.switchTo().alert();
            text = alert.getText();
            alert.accept();
            driver.switchTo().defaultContent();
            return text;
        } catch (Exception ex) {
            return "No popup box present.";
        }
    }

    /**
     * This function return the number of element found.
     *
     * @param by
     * @param driver
     * @return
     */
    public static int getNumberOfElementsFound(By by, WebDriver driver) {
        return driver.findElements(by).size();
    }

    /**
     * This function finds the web element.
     *
     * @param by
     * @param pos
     * @param driver
     * @return
     */
    public static WebElement getElementWithIndex(By by, int pos, WebDriver driver) {
        return driver.findElements(by).get(pos);
    }

    //FH
    /**
     * This function detects the malicious ads
     *
     * @param srcString
     * @return
     */
    public static boolean isMaliciousAd(String srcString) {
        BufferedReader br = null;
        try {
            File blackListFile = new File("blackList.txt");

            br = new BufferedReader(new FileReader(blackListFile));
            String currBlackUrl = br.readLine();
            //compare with each entry in the blackList file 
            while (currBlackUrl != null) {
                if (srcString.contains(currBlackUrl)) {
                    if (br != null) {
                        br.close();
                    }
                    return true;
                }
                currBlackUrl = br.readLine();
            }
            if (br != null) {
                br.close();
            }
            return false;
        } catch (FileNotFoundException ex) {
            //Logger.getLogger(CSE408_Project_2_Topic_2.class.getName()).log(Level.SEVERE, null, ex);
            out.println(ex.getMessage());
        } catch (IOException ex) {
            // Logger.getLogger(CSE408_Project_2_Topic_2.class.getName()).log(Level.SEVERE, null, ex);
            out.println(ex.getMessage());
        } finally {
            return false;
        }
    }
}
