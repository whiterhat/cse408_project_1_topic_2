The selenium-server-standalone-2.40.0.jar file was included in the library.
To run this program, first install chromedriver.exe, run and keep the window open. Then run the program CSE408_Project_1_Topic_2.java in IDE.

The program will open input.txt which contains the website test lists. They were selected to have relatively more number of iFrames. In order to test the full range of possible links our regular expression can match, some websites were selected to include source link that were meant to be displayed in a non-English language, tranlsated into ASCII representation. Example: http://??.tw/

Each URL will be opened through a new WebDriver. For each iFrame inside one website, our program will search for iFrame to click automatically.
The regular expressions is used to match all valid URLs. The reason is because the "src" or source attribute in iFrame can sometimes contain javascript source instead of clickable website link. 

